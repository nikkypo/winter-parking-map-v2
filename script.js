var map, view;

require([
  "esri/Map",
  "esri/views/MapView",
  "esri/widgets/Search",
  "esri/layers/FeatureLayer",
  "esri/portal/PortalItem",
  "esri/layers/Layer",
  "esri/widgets/Home",
  "esri/Graphic",
  "esri/widgets/Legend",
  "esri/widgets/Expand",
  "esri/symbols/PictureMarkerSymbol",
],

function (Map, MapView, Search, FeatureLayer, PortalItem, Layer, Home, Graphic, Legend, Expand, PictureMarkerSymbol) {
  map = new Map({
    basemap: "streets-navigation-vector",
  });
  view = new MapView({
    map: map,
    container: "viewDiv",
    center: [-93.109, 44.91396,],
    zoom: 10,
  });

  var marker = new PictureMarkerSymbol({
    url: "marker.png",
    width: 40,
    height: 40,
  });

  var search = new Search({
    view: view,
    popupEnabled: false,
  });
  search.watch("activeSource", function(evt){
    evt.resultSymbol = marker;
  });


  var boundary = new FeatureLayer({
    url: "https://services1.arcgis.com/9meaaHE3uiba0zr8/arcgis/rest/services/City/FeatureServer/0",
  });

  var winterParking = new FeatureLayer({
    url: "https://services1.arcgis.com/9meaaHE3uiba0zr8/ArcGIS/rest/services/WinterStreetParking/FeatureServer/0",
    // definitionExpression: "PlowRoute = 'Night Plow Route'",
  });

  var homeBtn = new Home({
    view: view,
  });

  var legend = new Expand({
    content: new Legend({
      view: view,
    }),
    view: view,
    expanded: false,
    container: "expandDiv",
    expandIconClass: "esri-icon-notice-round",
  });
  view.style = {
    type: "card",
    layout: "side-by-side",
  };
  view.ui.add(legend, "manual");

  map.add(winterParking);
  map.add(boundary);
  view.ui.add(homeBtn, "top-left");
  view.ui.add(search, "top-right");


  // Layer.fromPortalItem({
  //   portalItem: {
  //     id: "a3189775e0ea4f00aeabb83fbab53525"
  //   }
  // }).then(function(layer) {
  //   // Adds layer to the map
  //   map.add(layer);
  //   var x =JSON.parse(JSON.stringify(layer));
  //   console.log((x))
  // });

});
